<?php declare(strict_types=1);
/** Copyright © All rights reserved. */

namespace App;

class Indexer
{
    public const DEFAULT_PROVIDER = Adapter\Owid::class;

    /**
     * Retrieve the data from external provider
     * and index it in out own common format
     * @param string $provider
     */
    public static function index($provider = self::DEFAULT_PROVIDER)
    {
        /** @var Adapter\Contracts\AdapterInterface */
        $adapter = new $provider();
        $adapter->index();
    }
}

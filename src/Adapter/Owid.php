<?php declare(strict_types=1);
/** Copyright © All rights reserved. */

namespace App\Adapter;

class Owid extends Common
{
    public const DATA_URL = 'https://covid.ourworldindata.org/data/owid-covid-data.csv';

    /** @inheritDoc */
    public function index()
    {
        $fileHandler = fopen($this->getRawDataFilePath(), 'r');
        $day = 0;
        $parsedData = [];

        while (!feof($fileHandler)) {
            $line = fgets($fileHandler);

            // skip header
            if ($day == 0) {
                $day++;
                continue;
            }

            // skip blank lines
            if (!is_string($line)) {
                continue;
            }

            $columns = explode(',', $line);
            $date = $columns[3];
            $cases = floatval($columns[4]);

            $parsedData[$date] ??= 0;
            $parsedData[$date] += $cases;

            $day++;
        }

        // sort by date
        ksort($parsedData);

        // remove empty days at start
        foreach (array_keys($parsedData) as $key) {
            if (empty($parsedData[$key])) {
                unset($parsedData[$key]);
            } else {
                break;
            }
        }

        fclose($fileHandler);

        file_put_contents(
            $this->getRawDataFilePath('json'),
            json_encode($parsedData)
        );
    }
}

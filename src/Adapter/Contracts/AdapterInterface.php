<?php declare(strict_types=1);
/** Copyright © All rights reserved. */

namespace App\Adapter\Contracts;

interface AdapterInterface
{
    /** Index the raw data in our format */
    public function index();
}

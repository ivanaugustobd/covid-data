<?php declare(strict_types=1);
/** Copyright © All rights reserved. */

namespace App\Adapter;

abstract class Common implements Contracts\AdapterInterface
{
    public const RAW_DATA_FORMAT = 'csv';
    public const PARSED_DATA_FORMAT = 'json';
    public const DATA_URL = '<override this in your adapter>';

    public function execute()
    {
        if (self::DATA_URL === static::DATA_URL) {
            throw new \Exception('Set a valid DATA_URL constant on your adapter.');
        }

        if (!file_exists($this->getRawDataFilePath())) {
            $this->retrieveData(static::DATA_URL);
        }
    }

    /**
     * Retrieve the data from given URL
     *
     * @param string $url
     * @param string $format
     */
    public function retrieveData($url, $format = self::RAW_DATA_FORMAT)
    {
        $data = file_get_contents($url);
        file_put_contents($this->getRawDataFilePath($format), $data);
    }

    /**
     * Retrieve data file path
     * @param $format File format
     * @return string
     */
    protected function getRawDataFilePath($format = self::RAW_DATA_FORMAT)
    {
        return ROOT_DIR . '/tmp/' . "{$this->getCurrentAdapterName()}.$format";
    }

    /**
     * Retrieve data file path
     * @param $format File format
     * @return string
     */
    protected function getParsedDataFilePath($format = self::PARSED_DATA_FORMAT)
    {
        return ROOT_DIR . '/tmp/' . "{$this->getCurrentAdapterName()}.parsed.$format";
    }

    /**
     * Retrieve current adapter class name
     * @return string
     */
    protected function getCurrentAdapterName()
    {
        $classPathParts = explode('\\', static::class);

        return array_pop($classPathParts);
    }
}

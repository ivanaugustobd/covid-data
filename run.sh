#!/bin/bash

DAYS=$(test -n "$1" && echo "$1" || echo "1")
PROVIDER=$(test -n "$2" && echo "$2" || echo "Owid")

php run.php $DAYS $PROVIDER

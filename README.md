# Covid Data

## Install
```
git clone https://gitlab.com/ivanaugustobd/covid-data
cd covid-data
composer install
```

## Run
### Option #1, via run.sh:
```sh
./run.sh 4 # or any other number of days
```

### Option #2, via shell:
Open a psysh shell: `vendor/bin/psysh`, then:
```php
predict(4) // or any other number of days
```

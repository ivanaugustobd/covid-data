<?php declare(strict_types=1);
/** Copyright © All rights reserved. */

define('ROOT_DIR', __DIR__);
define('ADAPTER_NAME', $argv[2] ?? 'Owid');

if (!function_exists('predict')) {
    function predict($days = 1)
    {
        $rawData = file_get_contents(ROOT_DIR . '/tmp/' . ADAPTER_NAME . '.json');
        $data = json_decode($rawData, true);
        $numericIndexedData = array_values($data);

        for ($day = 1; $day <= $days; $day++) {
            echo "$day -> {$numericIndexedData[$day - 1]}" . PHP_EOL;
        }
    }
}
